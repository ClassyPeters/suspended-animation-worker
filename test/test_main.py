#system packages
import subprocess

def test_blender_path():
    command = ["blender", "-b", "-E", "help"]
    blender_process = subprocess.run(command, capture_output=True)
    assert "BLENDER_EEVEE" or "CYCLES" in str(blender_process.stdout)
