#system packages
import os, base64, time
from datetime import timedelta, datetime
import json
import threading
from threading import Thread, Event
import queue
import subprocess

#third party packages
from dotenv import load_dotenv
import jwt
import requests
import ffmpeg

def get_job_list():
    '''this function calls /node/job to get a list of jobs with files to be 
    downloaded, then passes that list into the download_file function/threads'''
    global api_host
    global headers
    r = requests.get(api_host + '/job/?status=started', headers=headers)
    if r.status_code == 200:
        jobs = json.loads(str(r.text)) 
        for job in jobs:
            download_queue.put(job)
    print("Download Queue is " + str(download_queue.qsize()) + " files long")

def download_file():
    '''this function downloads and saves blender files for rendering'''
    global api_host
    global headers
    while True:
        job = download_queue.get()
        if os.path.exists(os.path.join(os.getcwd(), "files", str(job["file"]) + ".blend")) == True:
            pass
        else:
            r = requests.get(api_host +"/files/" + str(job["file"]), headers=headers)
            print("status code for download is " + str(r.status_code))
            if r.status_code == 200:
                empty_file = open(os.path.join(os.getcwd(), "files", str(job["file"]) + ".blend"),'wb')
                empty_file.write(r.content)
                print("Downloaded file ", str(job["file"]), " successfully")
            else:
                print("Error ", str(r.status_code), str(r.text), " while trying to download file ", str(job["file"]))
        download_queue.task_done()

def get_job_files():
    '''this function returns the job files on disk for use in requests to the api''' 
    files_list = os.listdir(path='files')
    if '.gitkeep' in files_list: 
        files_list.remove('.gitkeep')
    for _ in files_list:
        if '.blend' not in _:
            files_list.remove(_)
    list_to_post = ''
    for _ in files_list:
        file = "file={}&".format(_.replace('.blend',''))
        list_to_post = list_to_post + file
    return list_to_post

def build_blender_command(input_file, output, file_type, render_engine, frame):
    '''building off of flamenco, this function returns a command
    used in the subprocess module to start blender processes'''
    command = ["blender","-b"]
    command.extend([input_file])
    command.extend(["--render-output", str(output)])
    command.extend(["--render-format", str(file_type)])
    command.extend(["--engine", str(render_engine)])
    command.extend(["--render-frame", str(frame)])
    return command

def run_ffmpeg(directory_string, frame_rate, output):
    (
    ffmpeg
    .input(directory_string, framerate=frame_rate)
    .output(output)
    .run(capture_stdout=True)
    )

def run_task():
    '''this function takes in a task object (after the file has been downloaded)
    and executes blender with lib/subprocess'''
    while True: 
        global interval
        global headers
        global api_host
        global fs_mount
        #poll /node/tasks over and over again until a task is returned
        files = get_job_files()
        if files == None:
            pass
        r = requests.get(api_host + '/task/?' + files + 'status=created', headers=headers)
        print("status code for task request was " + str(r.status_code)) 
        if r.status_code == 200:
            whole_task = json.loads(str(r.text))
            task = whole_task["task"]
            if whole_task["task_type"] == "render":
                print("starting blender")
                #the octal after 'frame' causes there to be no padding digits
                blender_process = subprocess.run(build_blender_command(os.path.join(os.getcwd(), "files/"+ str(task["file"])\
                    + ".blend"), "frame#", task["image_format"], task["render_engine"], task["frame"]), capture_output=True)
                print("blender finished")
                whole_task.update({"log": str(blender_process.stdout)})
            elif whole_task["task_type"] == "encode":
                directory = os.path.join(fs_mount, str(task["user"]), str(task["job"]))
                match_value = "/frame%d.png"
                directory_string = directory + match_value
                output = str(task["job"]) + str(task["frame_start"]) +"_" + str(task["frame_end"]) + ".mp4"
                encode_process = run_ffmpeg(directory_string, float(task["frame_rate"]), output)
                #whole_task.update({"log": str(encode_process)})
            upload_queue.put(whole_task)
        else:
            time.sleep(interval)
            
def upload_result():
    """uploads the result of running a specific task back to the API, the last function in a task's lifecycle"""
    while True:
        global api_host
        global headers
        result = upload_queue.get()
        task = result["task"]
        if result["task_type"] == "render":
            log_data = {"log": result["log"]}
            log_response = requests.post(api_host + '/task/render/' + str(task["id"]) + '/log', data=json.dumps(log_data), headers=headers)
            print("log response was ", log_response.status_code, log_response.text)
            files = {'file' : open(os.path.join('frame' + str(task["frame"]) + '.' \
                + str.lower(task["image_format"])), 'rb')}
            upload_response = requests.post(api_host + '/result/render/' + str(task["id"]), headers=headers, files=files)
            print("upload to results endpoint was ", upload_response.status_code, upload_response.text)
            if upload_response.status_code == 200:
                os.remove('frame' + str(task["frame"]) + '.' + str.lower(task["image_format"]))
                update_data = {"status": "complete"}
                update_response = requests.put(api_host + '/task/' + str(task["id"]), data=json.dumps(update_data), headers=headers)
            else:
                #reset the task's status to be picked up by another node
                update_data = {"status": "created"}
            print("update status was " + str(update_response.status_code), update_response.text)
        elif result["task_type"] == "encode":
            #log_data = {"log": result["log"]}
            #log_response = requests.post(api_host + '/task/encode/' + str(task["id"]) + '/log', data=json.dumps(log_data), headers=headers)
            #print("log response was ", log_response.status_code, log_response.text)
            files = {'file': open(os.path.join(str(task["job"]) + str(task["frame_start"]) +"_" + str(task["frame_end"]) + ".mp4"), 'rb')}
            upload_response = requests.post(api_host + '/result/encode/' + str(task["id"]), headers=headers, files=files)
        upload_queue.task_done()

#high level function for log printing

class poll_thread(Thread):
    def __init__(self, event):
        Thread.__init__(self)
        self.stopped = event

    def run(self):
        global interval
        get_job_list()
        while not self.stopped.wait(interval):
            get_job_list()

def update_token():
    global auth_token
    global headers
    r = requests.get(api_host + '/token/', headers=headers)
    if r.status_code == 200:
        response_json = json.loads(r.text)
        auth_token = response_json["token"] 
        headers = {'Authorization': 'Bearer ' + auth_token}
        print('Token updated successfully')
    else: 
        print('Token update failed')
        print(r.text)
        print(r.status_code)

def calculate_token_valid_time():
    global auth_token
    global headers
    decoded = jwt.decode(auth_token, verify=False)
    token_expiration = datetime.utcfromtimestamp(decoded['exp'])
    refresh_time = token_expiration - timedelta(minutes=1)
    #refresh the token 1 minute before it expires
    time_until_refresh = refresh_time - datetime.utcnow()
    seconds_until_refresh = time_until_refresh.total_seconds()
    return seconds_until_refresh

class token_update_thread(Thread):
    def __init__(self, event):
        Thread.__init__(self)
        self.stopped = event

    def run(self):
        while True:
            token_interval = calculate_token_valid_time()
            print('Token interval is ', token_interval)
            time.sleep(token_interval)
            update_token()

if __name__ == '__main__':
    #run global functions for obtaining token and host information
    load_dotenv()
    auth_token = str(os.getenv('auth_token', default=None)) 
    api_host = str(os.getenv('api_host', default='http://localhost') + '/node') 
    interval = int(os.getenv('interval', default=45)) 
    download_threads = int(os.getenv('download_threads', default=1))
    fs_mount = str(os.getenv('fs_mount', default=None))

    headers = {'Authorization': "Bearer " + auth_token}

    #define the queues used for thread safety/race conditions
    download_queue = queue.Queue()
    job_queue = queue.Queue()
    task_queue = queue.Queue()
    upload_queue = queue.Queue()

    #set information and start threads
    poll_flag = Event()
    poll = poll_thread(poll_flag)
    poll.start()

    #these threads are for keeping the token on file up to date. This will eventually be the standard OAuth token refresh flow
    token_flag = Event()
    token_thread = token_update_thread(token_flag) 
    token_thread.start()

    #these threads downloads files from the API
    for _ in range(download_threads):
        download_thread = threading.Thread(target=download_file)
        download_thread.start()

    #where some gosh darn magic happens
    task_thread = threading.Thread(target=run_task)
    task_thread.start()

    #send the results of rendering back to the API server
    upload_thread = threading.Thread(target=upload_result)
    upload_thread.start()
