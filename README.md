# riesling-worker

This is the software that runs on each worker node to connect to the main endpoint/scheduler (https://gitlab.com/ClassyWhetten/rieslingendpoint)

#### Features
- Multithreading to allow downloads, uploads, and rendering to occur asynchronously 
- Clean up of rendered files after the upload completes
- Token refresh flow for less hand holding of workers after they're set up
- Docker Image using LTS version of Blender 2.83

=======
The master branch of this repository aligns with the API endpoint repository

The workflow it follows is making a call to /node/files and compares the results of that list to what's been downloaded already. Any undownloaded files are then downloaded to the worker through /node/files/{file id}

Independent of that, there's another thread that at a specified interval sends a list of files downloaded to the API server asking if there are any tasks available to be worked on with the available files. If there are, it executes them and passes it off to another thread to upload the result of the task, update the task entry with the API, and remove the local copy of the file if that happens successfully.
