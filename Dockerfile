FROM debian:bullseye-slim AS blender_download 
#download and extract blender 
RUN apt-get update && \
	apt-get install --no-install-recommends -y \
		curl \
		ca-certificates \
		openssl \
		xz-utils 

#for alternate blender versions or builds, update this link
ENV BLENDER_TAR_URL https://mirror.clarkson.edu/blender/release/Blender2.83/blender-2.83.10-linux64.tar.xz
RUN mkdir /usr/local/bin/blender && \
	curl -Sl "$BLENDER_TAR_URL" -o blender.tar.xz && \
	tar -xf blender.tar.xz -C /usr/local/bin/blender && \
	rm blender.tar.xz

FROM python:3.7.12-slim-bullseye as worker_base
#TODO implement Nvidia CUDA

WORKDIR /app

RUN apt-get update && \
	apt-get install --no-install-recommends -y \
		libfreetype6 \
		libgl1-mesa-dev \
		libglu1-mesa \
		libxi6 \
		libxrender1 \
		ffmpeg

COPY --from=blender_download /usr/local/bin/blender /usr/local/bin/blender
COPY requirements.txt .
RUN pip install -r requirements.txt 

FROM worker_base as worker_final

COPY . .
RUN mkdir files
VOLUME /app/files
CMD ["python3", "worker.py"]
