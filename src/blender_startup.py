#this file can be loaded into blender to enable a few key features in a reusable way
#it enables CUDA and GPUs as CUDA rendering devices
#you're likely to get a crap ton of errors running it outside of blender

#I'm only targeting CUDA since until rocm comes around 

import bpy

def set_cycles_to_CUDA():
    bpy.context.preferences.addons["cycles"].preferences.compute_device_type = "CUDA"

def get_gpus():
    bpy.context.preferences.addons["cycles"].preferences.get_num_gpu_devices()
